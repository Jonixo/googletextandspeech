package com.furkan.databoss.text2speech.dto;

public class Speech2TextDTO {

    public String message;

    public Speech2TextDTO(String message, String convertedText) {
        this.message = message;
        this.convertedText = convertedText;
    }

    private String convertedText;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getConvertedText() {
        return convertedText;
    }

    public void setConvertedText(String convertedText) {
        this.convertedText = convertedText;
    }

}
