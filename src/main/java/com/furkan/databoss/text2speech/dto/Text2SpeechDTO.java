package com.furkan.databoss.text2speech.dto;

import java.io.File;

public class Text2SpeechDTO {

    public String message;
    public File file;

    public Text2SpeechDTO(String message, File file) {
        this.message = message;
        this.file = file;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
