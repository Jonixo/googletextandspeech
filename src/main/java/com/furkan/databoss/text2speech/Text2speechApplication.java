package com.furkan.databoss.text2speech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Text2speechApplication {

	public static void main(String[] args) {
		SpringApplication.run(Text2speechApplication.class, args);
	}
}
