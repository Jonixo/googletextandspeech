package com.furkan.databoss.text2speech.service;


import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.speech.v1.*;
import com.google.protobuf.ByteString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@Service
public class Speech2TextService {

    private static final String NEW_AUDIO_FILE_CANNOT_BE_GENERATED = "New file cannot be generated.";
    private static final String INPUT_PATH_IS_A_DIRECTORY = "Input path is a directory!";
    private static final String TR_LANGUAGE_CODE = "tr-TR";
    private static final int SAMPLE_RATE_HERTZ = 16000;

    @Value("${google.auth.json_path}")
    String authPath;

    public String speech2text(File f) {

        if (f.exists() && !f.isDirectory()) {
            try {


                FileInputStream credentialsStream = new FileInputStream(authPath);
                GoogleCredentials credentials = GoogleCredentials.fromStream(credentialsStream);
                FixedCredentialsProvider credentialsProvider = FixedCredentialsProvider.create(credentials);

                SpeechSettings speechSettings =
                        SpeechSettings.newBuilder()
                                .setCredentialsProvider(credentialsProvider)
                                .build();

                SpeechClient speech = SpeechClient.create(speechSettings);


                StringBuilder output = null;
                Path path = Paths.get(f.getAbsolutePath());
                byte[] data = Files.readAllBytes(path);
                ByteString audioBytes = ByteString.copyFrom(data);

                // Configure request with local raw PCM audio
                RecognitionConfig config =
                        RecognitionConfig.newBuilder()
                                .setEncoding(RecognitionConfig.AudioEncoding.LINEAR16)
                                .setLanguageCode(TR_LANGUAGE_CODE)
                                .setSampleRateHertz(SAMPLE_RATE_HERTZ)
                                .build();
                RecognitionAudio audio = RecognitionAudio.newBuilder().setContent(audioBytes).build();

                // Use blocking call to get audio transcript
                RecognizeResponse response = speech.recognize(config, audio);
                List<SpeechRecognitionResult> results = response.getResultsList();

                for (SpeechRecognitionResult result : results) {
                    // There can be several alternative transcripts for a given chunk of speech. Just use the
                    // first (most likely) one here.
                    SpeechRecognitionAlternative alternative = result.getAlternativesList().get(0);

                    if (output == null)
                        output = Optional.ofNullable(alternative.getTranscript()).map(StringBuilder::new).orElse(null);
                    else
                        output.append(alternative.getTranscript());
                }
                return output == null ? null : output.toString();
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        } else if (!f.exists())
            throw new IllegalStateException(NEW_AUDIO_FILE_CANNOT_BE_GENERATED);
        else
            throw new IllegalArgumentException(INPUT_PATH_IS_A_DIRECTORY);
    }


}
