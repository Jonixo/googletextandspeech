package com.furkan.databoss.text2speech.service;

import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.texttospeech.v1.*;
import com.google.protobuf.ByteString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

@Service
public class Text2SpeechService {

    @Value("${google.auth.json_path}")
    String authPath;

    public byte[] googleText2Speech(String text) throws IOException {

        FileInputStream credentialsStream = new FileInputStream(authPath);
        GoogleCredentials credentials = GoogleCredentials.fromStream(credentialsStream);
        FixedCredentialsProvider credentialsProvider = FixedCredentialsProvider.create(credentials);


        TextToSpeechSettings textToSpeechSettings =
                TextToSpeechSettings.newBuilder()
                        .setCredentialsProvider(credentialsProvider)
                        .build();

        TextToSpeechClient textToSpeechClient = TextToSpeechClient.create(textToSpeechSettings);
        // Set the text input to be synthesized
        SynthesisInput input = SynthesisInput.newBuilder().setText(text).build();

        // Build the voice request; languageCode = "tr-TR"
        VoiceSelectionParams voice = VoiceSelectionParams.newBuilder().setLanguageCode("tr-TR")
                .setSsmlGender(SsmlVoiceGender.FEMALE)
                .build();

        // Select the type of audio file you want returned
        AudioConfig audioConfig = AudioConfig.newBuilder().setAudioEncoding(AudioEncoding.MP3) // MP3 audio.
                .build();

        // Perform the text-to-speech request
        SynthesizeSpeechResponse response = textToSpeechClient.synthesizeSpeech(input, voice, audioConfig);

        // Get the audio contents from the response
        ByteString audioContents = response.getAudioContent();

         return audioContents.toByteArray();
    }



}
