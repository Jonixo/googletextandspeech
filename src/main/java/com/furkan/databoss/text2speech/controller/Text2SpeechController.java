package com.furkan.databoss.text2speech.controller;

import com.furkan.databoss.text2speech.dto.Speech2TextDTO;
import com.furkan.databoss.text2speech.service.Speech2TextService;
import com.furkan.databoss.text2speech.service.Text2SpeechService;
import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.texttospeech.v1.*;
import com.google.protobuf.ByteString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.*;


@RestController
@CrossOrigin
@RequestMapping("/textandspeech")
public class Text2SpeechController {


    private static final String SPEECH_TO_TEXT_SUCCESSFUL = "Speech to text conversion is successful.";
    private static final String FIELD_CANNOT_BE_EMPTY = "Text field cannot be empty";
    private static final String CONTENT_DISPOSITION = "Content-Disposition";
    private static final String MEDIA_TYPE = "audio/mp3";
    private final Text2SpeechService text2SpeechService;
    private final Speech2TextService speech2TextService;
    private final Util utilities;

    @Autowired
    public SpeechAndTextController(Text2SpeechService text2SpeechService, Speech2TextService speech2TextService, Util utilities) {
        this.text2SpeechService = text2SpeechService;
        this.speech2TextService = speech2TextService;
        this.utilities = utilities;
    }



    @PostMapping("/t2s")
    public void googleTexttoSpeech(HttpServletResponse httpResponse, @Valid @RequestParam("text") String text) {
        if (text == null || text.isEmpty());

        else {
            try {
                byte[] audioByte=text2SpeechService.googleText2Speech(text);
                httpResponse.setContentType(MEDIA_TYPE);
                httpResponse.setContentLength((int) audioByte.length);
                httpResponse.getOutputStream().write(audioByte);

            } catch (RuntimeException | IOException e) {

            }
        }
    }

    //input 'filePathString' must be whole path without file type ex:/home/user/Downloads/soundfile
    @PostMapping("/s2t")
    public Speech2TextDTO googleSpeechtoText(@Valid @RequestParam("file") MultipartFile multipartFile) {
        try {


            File file = utilities.multipartToFile(multipartFile);
            String convertedMessage = speech2TextService.speech2text(file);
            utilities.delete(file);

            return new Speech2TextDTO(SPEECH_TO_TEXT_SUCCESSFUL, convertedMessage);

        } catch (IllegalStateException | IOException e) {
            throw new CustomException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (IllegalArgumentException e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (RuntimeException e) {
            throw new CustomException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}

